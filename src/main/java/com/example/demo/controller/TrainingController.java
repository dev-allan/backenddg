package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Training;
import com.example.demo.repository.TrainingRepository;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/api")
public class TrainingController {

	@Autowired
	TrainingRepository trainingRepository;

	@GetMapping("/notifications")
	public ResponseEntity<List<Training>> getAllNotification(@RequestParam(required = false) String title) {
		try {
			List<Training> training = new ArrayList<Training>();

			if (title == null)
				trainingRepository.findAll().forEach(training::add);
			else
				trainingRepository.findByTitleContaining(title).forEach(training::add);

			if (training.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(training, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/notifications/{id}")
	public ResponseEntity<Training> getNotificationById(@PathVariable("id") long id) {
		Optional<Training> trainingData = trainingRepository.findById(id);

		if (trainingData.isPresent()) {
			return new ResponseEntity<>(trainingData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/notifications")
	public ResponseEntity<Training> createNotification(@RequestBody Training training) {
		try {
			Training _training = trainingRepository
					.save(new Training(training.getTitle(), training.getDescription(), training.getTheme()));
			return new ResponseEntity<>(_training, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/notifications/{id}")
	public ResponseEntity<Training> updateNotification(@PathVariable("id") long id, @RequestBody Training training) {
		Optional<Training> trainingData = trainingRepository.findById(id);

		if (trainingData.isPresent()) {
			Training _training = trainingData.get();
			_training.setTitle(training.getTitle());
			_training.setDescription(training.getDescription());
			_training.setTheme(training.getTheme());
			return new ResponseEntity<>(trainingRepository.save(_training), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/notifications/{id}")
	public ResponseEntity<HttpStatus> deleteNotification(@PathVariable("id") long id) {
		try {
			trainingRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/notifications")
	public ResponseEntity<HttpStatus> deleteAllNotification() {
		try {
			trainingRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
