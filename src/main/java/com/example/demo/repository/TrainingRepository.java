package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Training;

public interface TrainingRepository extends JpaRepository<Training, Long> {

  List<Training> findByTitleContaining(String title);
}