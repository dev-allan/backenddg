package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "tutorials")
public class Training {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "theme")
	private String theme;

	public Training() {

	}

	public Training(String title, String description, String theme) {
		this.title = title;
		this.description = description;
		this.theme = theme;
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String getTheme) {
		this.theme = getTheme;
	}

	@Override
	public String toString() {
		return "Tutorial [id=" + id + ", title=" + title + ", desc=" + description + ", theme=" + theme + "]";
	}
}